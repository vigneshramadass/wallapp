# ![The Wall App]


## Getting started


To get the frontend running locally:

- Clone this repo
- `npm install` to install all req'd dependencies
- make sure the backend api running on localhost 
- `npm start` to start the local server (this project uses create-react-app)

Local web server will use port 4100 instead of standard React's port 3000 to prevent conflicts with some backends like Node or Rails. You can configure port in scripts section of `package.json`


### backend API
- Create a Python virtual environment under /server. You can follow this guide.
- Activate the environment: source env/bin/activate
- Install dependencies: pip install -r requirements.txt
- Now you're ready to run the dev server:
  - python manage.py migrate
  - python manage.py runserver



**General functionality:**

- Authenticate users via JWT (login/signup pages + logout button on settings page)
- sign up & settings page - no deleting.
- Create Articles
- Comments on articles (no updating)
- GET and display paginated lists of articles
- Favorite articles

**The general page breakdown looks like this:**
- Home page (URL: /#/ )
    - List of articles (click on Global Feed)
- Sign in/Sign up pages (URL: /#/login, /#/register )
    - Use JWT (store the token in localStorage)
- Settings page (URL: /#/settings )
- Editor page to create/edit articles (URL: /#/editor)
- Article page (URL: /#/article/article-slug-here )
    - My Article(only shown to article's author)
    - Edit article.
    - comment on article
- Profile page (URL: /#/@username, /#/@username/favorites )
    - Show basic user info
    - List of articles populated from author's created articles or author's favorited articles

<br />