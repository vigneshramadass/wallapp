from django.apps import AppConfig


class AuthenticationAppConfig(AppConfig):
    name = 'src.apps.authentication'
    label = 'authentication'
    verbose_name = 'Authentication'

    def ready(self):
        import src.apps.authentication.signals


default_app_config = 'src.apps.authentication.AuthenticationAppConfig'
