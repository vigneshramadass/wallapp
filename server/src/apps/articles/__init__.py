from django.apps import AppConfig


class ArticlesAppConfig(AppConfig):
    name = 'src.apps.articles'
    label = 'articles'
    verbose_name = 'Articles'

    def ready(self):
        import src.apps.articles.signals

default_app_config = 'src.apps.articles.ArticlesAppConfig'
